from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models


def index(request):
    context = {}
    context["tasks"] = models.Task.objects.all()
    return render(request, "todo/index.html", context)

def add_task(request):
    if "text" in request.POST:
        task = models.Task()
        task.text = request.POST["text"]
        task.save()
    return redirect('index')

def change_status(request,id):
    if "status" in request.POST:
        task = models.Task.objects.get(id=id)
        task.status = request.POST["status"]
        task.save()
    return redirect('index')

def delete_task(request,id):
    task= models.Task.objects.get(id=id)
    task.delete()
    return redirect('index')
