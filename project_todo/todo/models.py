from django.db import models

status_choices = [
    ('T', 'Todo'),
    ('D', 'Done'),
    ('C', 'Canceled'),
]

class Task(models.Model):
    text = models.CharField(max_length=200)
    status = models.CharField(max_length=1, choices=status_choices, default="T")
    
    def status_translation(self):
        status_texts = {
                "T": "Todo",
                "D": "Erledigt",
                "C": "Gecancelt",
                }
        return status_texts[self.status]
    
    def __str__(self):
        return f"{self.text} {self.status[0]}"



